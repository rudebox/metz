<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//text
	$text = get_field('page_text');

	//product img
	$product = get_field('product_img');
?>


<section class="page__hero">
	<div class="wrap hpad">
		<div class="row page__row">
			<div class="col-sm-6">
				<h1 class="page__title wow fadeInLeft"><?php echo $title; ?></h1>
				<?php  if ($text) : ?>
				<p class="page__text wow fadeInLeft"><?php echo $text; ?></p>
				<?php endif; ?>
			</div>	

			<?php if ($product) : ?>			
			<div class="page__product col-sm-6">
				<img src="<?php echo $product['url']; ?>" alt="<?php echo esc_attr($product['alt']); ?>">	
			</div>			
			<?php endif; ?>
		</div>
	</div>
</section>
